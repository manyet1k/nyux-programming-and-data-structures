#include <iostream>
#include <string>
using namespace std;
void capitalize(string& x){
    for (int i=0; i<x.length(); i++){
        x[i]=toupper(x[i]);
    }
}
bool isInTheArray(string x, string array1[]){
    for (short i=0; i<5; i++){
        if (array1[i] == x)
            return 1;
    }
    return 0;
}
int main(){
    string weekdays[5] = {"MON","TUE","WED","THU","FRI"}, weekends[2] = {"SAT","SUN"}, day; short time, minutes; float price;
    while(!isInTheArray(day, weekdays) && !isInTheArray(day, weekdays)){
        cout<<"Enter the day the call started at: "; cin>>day; capitalize(day);
        if(!isInTheArray(day, weekdays) && !isInTheArray(day, weekdays))
            cout<<"Please enter the first 3 letters of the day\n";
    }
    while(!(time<2400)){
        cout<<"Enter the time the call started at (hhmm): "; cin>>time;
        if(!(time<2400))
            cout<<"Please enter the time in the 24h format\n";
    }
    cout<<"Enter the duration of the call (in minutes): "; cin>>minutes;
    cout<<"This call will cost $";
    if(isInTheArray(day,weekends))
        price = 0.15*minutes;
    else{
        if(time>800 && time<1800)
            price = 0.4*minutes;
        else
            price = 0.25*minutes;
    }
    cout<<price<<endl;
}