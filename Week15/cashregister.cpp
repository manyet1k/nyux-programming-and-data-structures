#include <iostream>
#include <math.h>
using namespace std;
double min(double x, double y){
    if (x<y)
        return x;
    return y;
}
double max(double x, double y){
    if (x>y)
        return x;
    return y;
}
int main(){
    double firstItem, secondItem, noTaxPrice; float taxRate; bool clubCard; char clubCardChar;
    cout<<"Enter price of the first item: "; cin>>firstItem;
    cout<<"Enter price of the second item: "; cin>>secondItem;
    a: cout<<"Does customer have a club card? (Y/N): "; cin>>clubCardChar;
    if (clubCardChar == 'y' || clubCardChar == 'Y')
        clubCard = 1;
    else if (clubCardChar == 'n' || clubCardChar == 'N')
       clubCard = 0;
    else {
        cout<<"Your input must be either Y for YES or N for NO\n";
        goto a;
    }
    cout<<"Enter tax rate, e.g. 5.5 for 5.5% tax: "; cin>>taxRate;
    taxRate /= 100; taxRate += 1;
    cout<<"Base price = "<<firstItem+secondItem<<endl;
    cout<<"Price after discounts = ";
    if (clubCard)
        noTaxPrice = 9 * (max(firstItem,secondItem) + min(firstItem,secondItem) / 2) / 10;
    else
        noTaxPrice = max(firstItem,secondItem) + min(firstItem,secondItem) / 2;
    cout<<noTaxPrice<<endl;
    cout<<"Total price = "<< ceil(noTaxPrice * taxRate * 100)/100<<endl;

}