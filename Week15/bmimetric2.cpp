#include <iostream>
#include <iomanip>
using namespace std;
int main(){
    float weight, height, bmi;
    cout<<"Please  enter weight in kilograms: "; cin>>weight;
    cout<<"Please  enter height in meters: "; cin>>height;
    bmi = weight / (height * height);
    cout<<"BMI is: "<<setprecision(4)<<bmi<<", Status is ";
    if (bmi<18.5)
        cout<<"Underweight\n";
    else if (bmi<25.0)
        cout<<"Normal\n";
    else if (bmi<30.0)
        cout<<"Overweight\n";
    else
        cout<<"Obese\n";
}