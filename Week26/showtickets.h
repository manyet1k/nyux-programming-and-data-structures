//#include <bits/stdc++.h>
//using namespace std;
class ShowTickets{
    private:
        vector<string> signatures;
        vector<bool> sold_status;
    public:
        bool is_sold(string row, string seat){
            for(short i=0; i<signatures.size(); i++)
                if(signatures[i] == row+seat)
                    return sold_status[i];
            return 0;
        }
        void sell_seat(string row, string seat){
            for(short i=0; i<signatures.size(); i++)
                if(signatures[i] == row+seat){
                    sold_status[i] = 1;
                    return;
                }
            signatures.push_back(row+seat);
            sold_status.push_back(1);
        }
        string print_ticket(string row, string seat){
            for(short i=0; i<signatures.size(); i++)
                if(signatures[i] == row+seat)
                    if(sold_status[i] == 0)
                        return row+' '+seat+" available";
                    else //if (sold_status[i] == 1)
                        return row+' '+seat+" sold";
            signatures.push_back(row+seat);
            sold_status.push_back(0);
            return row+' '+seat+" available";
        }
};
/*int main(){
    ShowTickets myticket;
    if(!myticket.is_sold("AA","101"))
        myticket.sell_seat ("AA","101");
    cout << myticket.print_ticket("AA","101") << endl;
    cout << myticket.print_ticket("AA","102") << endl;
    return 0;
}*/