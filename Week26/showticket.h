//#include <bits/stdc++.h>
//using namespace std;
class ShowTicket{
    protected:
        string row;
        string seat_number;
        bool sold_status;
    public:
        ShowTicket(): row("é"), seat_number("-1"), sold_status(0){}
        ShowTicket(string s, string s2): row(s), seat_number(s2), sold_status(0){}
        string print_ticket(){
            if(sold_status == 0)
                return row+' '+seat_number+" available";
            else //if(sold_status == 1)
                return row+' '+seat_number+" sold";
        }
        bool is_sold(){
            if(sold_status == 0)
                return 0;
            else //if(sold_status == 1)
                return 1;
        }
        void sell_seat(){
            if(sold_status == 0)
                sold_status = 1;
            else if(sold_status == 1)
                cout<<"Exception occured: Seat already sold\n";
        }
};
/*int main(){
    ShowTicket myticket1("AA","101");
    ShowTicket myticket2("AA","102");
    if(!myticket1.is_sold())
        myticket1.sell_seat();
    cout << myticket1.print_ticket() << endl;
    cout << myticket2.print_ticket() << endl;
    return 0;
}*/