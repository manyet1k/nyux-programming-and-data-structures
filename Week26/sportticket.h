//#include <bits/stdc++.h>
//#include "showticket.h"
//using namespace std;
class SportTicket: public ShowTicket{
    private:
        bool beer_soldStatus;
    public:
        SportTicket(){
            row = "é";
            seat_number = "-1";
            sold_status = "0";
            beer_soldStatus = "0";
        }
        SportTicket(string s, string s2){
            row = s;
            seat_number = s2;
            sold_status = 0;
            beer_soldStatus = 0;
        }
        bool beer_sold(){
            if (beer_soldStatus == 0)
                return 0;
            else //if (beer_soldStatus == 1)
                return 1;
        }
        void sell_beer(){
            if (beer_soldStatus == 0)
                beer_soldStatus = 1;
            else //if (beer_soldStatus == 1)
                cout<<"Exception occured: Beer already sold";
        }
        string print_ticket(){
            if(sold_status == 0)
                return row+' '+seat_number+" available";
            else //if(sold_status == 1)
                if(beer_soldStatus == 0)
                    return row+' '+seat_number+" sold nobeer";
                else //if(beer_soldStatus == 1)
                    return row+' '+seat_number+" sold beer";
        }
};
/*int main(){
    SportTicket myticket1("AA","101");
    SportTicket myticket2("AA","102");
    myticket1.sell_seat();
    myticket2.sell_seat();
    myticket2.sell_beer();
    cout << myticket1.print_ticket() << endl;
    cout << myticket2.print_ticket() << endl;
    return 0;
}*/