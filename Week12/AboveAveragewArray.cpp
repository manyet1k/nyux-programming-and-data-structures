#include <iostream>
using namespace std;
const int MAX_STUDENTS_IN_CLASS = 60;
int main(){
    short grades[MAX_STUDENTS_IN_CLASS]; float mean=0;
    cout<<"Please enter the number of students (no more than "<<MAX_STUDENTS_IN_CLASS<<") : "; int numOfStudents; cin>>numOfStudents;
    while(numOfStudents > MAX_STUDENTS_IN_CLASS){
        cout<<"The number of students can't be greater than "<<MAX_STUDENTS_IN_CLASS<<", please enter again: "; cin>>numOfStudents;
    }
    cout<<"Please enter the grades of the students, separated by spaces: ";
    for(int i=0; i<numOfStudents; i++){
        cin>>grades[i];
    }
    for(int i=0; i<numOfStudents; i++){
        mean += grades[i];
    }
    mean /= numOfStudents;
    cout<<"The mean grade is "<< mean << endl;
    cout<<"The grades that are larger than the average: \n";
    for(int i=0; i<numOfStudents; i++){
        if(grades[i]>mean)
            cout<<grades[i]<<" ";
    }
    cout<<endl;
}