#include <iostream>
using namespace std;
int main(){
    short arr[6];
    arr[0] = 4;
    arr[2] = 5;
    // arr[-1] = 3; You'll never want to do this. You'll WISH that this causes a core dump
    // arr[6] - 3; Same with this
    // short x=7; int arr2[x]; won't work
    const short x=7; short arr2[x]; //Will work 
    cout<<arr; //Will print the memory address of arr
    short arr3[6] = {5,7,6,2,1,15};
    short arr3[6] = {5,7,6}; //Will fill the rest of the array with 0s
}