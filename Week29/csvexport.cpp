//#include <bits/stdc++.h>
//using namespace std;
void csv_export(string data[][10], int records,int columns, string filename){
    ofstream outfile(filename);
    string s="";
    for(int i=0; i<records; i++){
        for(short o=0; o<columns; o++){
            s.append(data[i][o]);
            if(o != columns-1)
                s.append(",");
        }
        s.append("\n");
    }
    outfile << s;
    outfile.close();
}
/*int main(){
    string data[10][10] = {{"aspen","olmsted","aspen@pleasedonotemail.com"},{"sally","jones","sally@gmail.com"},{"fred","smith","fsmith@aol.com"}};
    csv_export(data,3,3,"customers.csv");
}*/