void swap(int *xp, int *yp){
    int temp = *yp;
    *yp = *xp;
    *xp = temp;
}