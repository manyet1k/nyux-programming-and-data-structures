//#include <iostream>
//#include "cpluspluslabs.h"
//using namespace std;
void selection_sort(int arr[], int elements){
    int minimum, minindex;
    for(int i=0; i<elements-1; i++){
        minimum = arr[i+1]; minindex = i+1;
        for(int o=i+1; o<elements; o++){
            if(arr[o]<minimum){
                minimum = arr[o];
                minindex = o;
            }
        }
        if(minimum<arr[i]){
            arr[minindex] = arr[i];
            arr[i]=minimum;
            swap(&minimum, &arr[i]);
        }
    }/*
    for(int i=0; i<elements; i++){
        cout<<arr[i]<<' ';
    }*/
}
/*int main(){
    int lst[] = {19, 2, 20, 1, 0, 18};
    selection_sort(lst, 6);
    
    cout<<'\n';
}*/