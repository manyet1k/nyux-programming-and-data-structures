#include <bits/stdc++.h>
using namespace std;
int main(){
    int *ptr = new int;
    //If you don't delete once you're done with the variable, a memory leak will occur
    delete ptr; ptr = NULL; //Set the pointer to NULL right after deleting to prevent double deleting
    int *willCrash = new int; delete willCrash; delete willCrash; //Crash go brrrrrr
}