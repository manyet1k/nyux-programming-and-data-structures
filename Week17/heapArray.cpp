#include <bits/stdc++.h>
using namespace std;
void expandArray(int*& firstArray, int newSize){
    int* tempArray = new int[newSize];
    for(int i=0; i<sizeof(firstArray)/sizeof(firstArray[0]); i++)
        tempArray[i] = firstArray[i];
    for(int i=sizeof(firstArray)/sizeof(firstArray[0]); i<newSize; i++)
        tempArray[i] = 0;
    delete firstArray;
    firstArray = tempArray;
}

int main(){
    int arrLength, temp, newSize; cout<<"Please enter array length: "; cin>>arrLength;
    int *arr = new int[arrLength]; int *arrCopy = arr;
    for(int i=0; i<arrLength; i++){
        cout<<"Please enter an array value for index " << i << ": "; cin>>temp;
        arr[i] = temp;
    } a:
    cout<<"Please enter a new size for the array to expand: "; cin>>newSize;
    if(arrLength>=newSize){
        cout<<"Please enter a value that is greater than the first length\n";
        goto a;
    }
    expandArray(arr, newSize);
    for(int i=arrLength; i<newSize; i++){
        cout<<"Please enter an array value for index " << i << ": "; cin>>temp;
        arr[i] = temp;
    }
    cout<<"The elements of the array are: ";
    for(int i=0; i<newSize; i++){
        cout<<arr[i];
        if(i!=newSize-1)
            cout<<", ";
    }
    cout<<"\n";
    delete[] arr; arr = NULL; //Deleting individual items from an array can be dangerous
}