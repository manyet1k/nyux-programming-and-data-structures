#include <bits/stdc++.h>
using namespace std;
template<class T> class BSTNode{
    public:
        T data;
        BSTNode* parent=nullptr;
        BSTNode* leftChild=nullptr;
        BSTNode* rightChild=nullptr;

        bool isLeaf(){
            if(leftChild == nullptr && rightChild == nullptr)
                return 1;
            return 0;
        }
        int size(){
            if (this == nullptr)
                return 0;
            int count = 1;
            if(leftChild != nullptr)
                count += leftChild->size();
            if(rightChild != nullptr)
                count += rightChild->size();
            return count;
        }
        void printInOrder(){
            if(this != nullptr){
                this->leftChild->printInOrder();
                cout<< this->data <<", ";
                this->rightChild->printInOrder();
            }
        }
        void printPreOrder(){
            if(this != nullptr){
                cout<< this->data <<", ";
                this->left.printInOrder();
                this->right.printInOrder();
            }
        }
        void printPostOrder(){
            if(this != nullptr){
                this->left.printInOrder();
                this->right.printInOrder();
                cout<< this->data <<", ";
            }
        }
};
template<class T> class BST{
    private:
        BSTNode<T>* root = nullptr;
    public:
        int size(){
            return root->size();
        }
        void printInOrder(){
            root->printInOrder();
        }
        void printPreOrder(){
            root->printPreOrder();
        }
        void printPostOrder(){
            root->printPostOrder();
        }
        void BreadthFirstPrint(){
            queue<BSTNode<T>*> q;
            q.push(root);
            BSTNode<T>* temp;
            while (!q.empty()){
                temp = q.front();
                cout<<temp->data<<", ";
                q.pop();
                if(temp->leftChild != nullptr)
                    q.push(temp->leftChild);
                if(temp->rightChild != nullptr)
                    q.push(temp->rightChild);
            }
        }
        void insert(T insertionData){
            BSTNode<T>* newNode = new BSTNode<T>;
            newNode->data = insertionData;
            newNode->leftChild = nullptr;
            newNode->rightChild = nullptr;
            if(this->size() == 0){
                root = newNode;
                return;
            }
            BSTNode<T>* temp = root;
            BSTNode<T>* prev;
            while(temp != nullptr){
                prev = temp;
                if(insertionData < temp->data)
                    temp = temp->leftChild;
                else
                    temp = temp->rightChild;
            }
            if(insertionData < prev->data){
                prev->leftChild = newNode;
                newNode->parent = prev;
            } else{
                prev->rightChild = newNode;
                newNode->parent = prev;
            }
        }
        void pop(T deletionData){
            BSTNode<T>* temp = root;
            while(deletionData != temp->data){
                if(temp == nullptr){
                    cout<<"Error: Node with value "<<deletionData<<" does not exist";
                    return;
                }
                if(deletionData < temp->data)
                    temp = temp->leftChild;
                else
                    temp = temp->rightChild;
            }
            if(temp->leftChild == nullptr && temp->rightChild == nullptr)
                if(temp->parent->data > temp->data)
                    temp->parent->leftChild = nullptr;  
                else
                    temp->parent->rightChild = nullptr;

            else if(temp->leftChild == nullptr || temp->rightChild == nullptr)
                if(temp->rightChild == nullptr)
                    if(temp->parent->data > temp->data)
                        temp->parent->leftChild = temp->leftChild;
                    else
                        temp->parent->rightChild = temp->leftChild;
                else
                    if(temp->parent->data > temp->data)
                        temp->parent->leftChild = temp->rightChild;
                    else
                        temp->parent->rightChild = temp->rightChild;

            else{
                BSTNode<T>* upperBound = temp->rightChild;
                while(upperBound->leftChild != nullptr)
                    upperBound = upperBound->leftChild;
                temp->data = upperBound->data;
                if(upperBound->parent->leftChild == upperBound)
                    upperBound->parent->leftChild = nullptr;
                else
                    upperBound->parent->rightChild = nullptr;
            }
        }
};
int main(){
    BST<int> x;
    x.insert(20);
    x.insert(5);
    x.insert(30);
    x.insert(3);
    x.insert(10);
    x.insert(25);
    x.printInOrder();
    printf("\n");
    x.pop(20);
    x.BreadthFirstPrint();
    printf("\n");
    cout<<x.size()<<'\n';
}