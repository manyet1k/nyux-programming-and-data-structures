#include <bits/stdc++.h>
#include "cpluspluslabs.h"
using namespace std;
int globalNumberOfRecords;
void csv_import2(NODE** data, string filename){
    string data1[10][10];
    ifstream infile(filename);
    int numberOfRecords=0; string line; int nthColumn;
    stringstream lineStream;
    string cell;
    while(getline(infile, line)){
        nthColumn=0;
        lineStream.clear();
        lineStream<<line;
        while(getline(lineStream, cell, ',')){
            data1[numberOfRecords][nthColumn]=cell;
            nthColumn++;
        }
        numberOfRecords++;
    }
    infile.close(); globalNumberOfRecords = numberOfRecords;

    NODE* newnode = new NODE; NODE* temp = LL.head;
    for(int i=0; i<numberOfRecords; i++){ //This could've implemented in a shorter way
        newnode = new NODE;
        newnode->firstname = data1[i][0];
        newnode->lastname = data1[i][1];
        newnode->email = data1[i][2];
        newnode->next = nullptr;

        if(LL.head == nullptr){
            LL.head = newnode;
            temp = LL.head;
            continue;
        }
        temp->next = newnode;
        temp = temp->next;
    }
}
int main(){
    NODE* data = nullptr; 
    csv_import2(&data,"customers.csv");

    NODE* temp = LL.head;
    while(globalNumberOfRecords--){
        cout<< temp->firstname <<' ' << temp->lastname <<' ' << temp->email <<'\n';
        temp = temp->next;
    }
}