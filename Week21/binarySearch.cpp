#include <bits/stdc++.h>
using namespace std;
int search(vector<int> vec, int val){
    int omittedIndexes = 0;
    if(val==vec[vec.size()/2-1])
        return vec.size()/2-1;
    if (vec[vec.size()/2]<val){
        omittedIndexes += vec.size()/2 + 1;
        return search(vector<int>(vec.begin()+vec.size()/2+1, vec.end()), val);
    } else if (vec[vec.size()/2]>val){
        omittedIndexes += vec.size()/2;
        return search(vector<int>(vec.begin(), vec.begin()+vec.size()/2-1), val);
    } else return vec.size()/2+omittedIndexes;
}
int main(){
    int x; cout<<"Please enter how many integers you'd like to enter: "; cin>>x;
    vector<int> vec; int temp;
    cout<<"All integers must be in ascending order, otherwise the program will fail!\n";
    for(int i=0; i<x; i++){
        cout<<"Please enter the value for index "<<i+1<<": "; cin>>temp; vec.push_back(temp);
    }
    int searchValue; cout<<"Please enter the value you'd like to search for: "; cin>>searchValue;
    searchValue = search(vec, searchValue);
    if(searchValue == -1)
        cout<<"Your search value isn't present in the array\n";
    else
        cout<<"Your search value is in the index "<<searchValue+1<<" of the array\n";
}