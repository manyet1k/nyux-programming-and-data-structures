#include <bits/stdc++.h>
using namespace std;
int search(int arr[], int size, int val){
    for(int i=0; i<size; i++){
        if(arr[i]==val)
            return i;
    }
    return -1;
}
int main(){
    int x; cout<<"Please enter how many integers you'd like to enter: "; cin>>x;
    int arr[x];
    for(int i=0; i<x; i++){
        cout<<"Please enter the value for index "<<i+1<<": "; cin>>arr[i];
    }
    int searchValue; cout<<"Please enter the value you'd like to search for: "; cin>>searchValue;
    searchValue = search(arr, x, searchValue);
    if(searchValue == -1)
        cout<<"Your search value isn't present in the array\n";
    else
        cout<<"Your search value is in the index "<<searchValue+1<<" of the array\n";
}