#include <bits/stdc++.h>
using namespace std;
int search(vector<int> vec, int val){
    int low=0, high=vec.size()-1, mid=(low+high)/2, index;
    while(1){
        if(val<vec[mid]){
            high = mid-1;
            mid = (low+high)/2;
        } else if(val>vec[mid]){
            low = mid+1;
            mid = (low+high)/2;
        } else return mid;
    }
}
int main(){
    int x; cout<<"Please enter how many integers you'd like to enter: "; cin>>x;
    vector<int> vec; int temp;
    cout<<"All integers must be in ascending order, otherwise the program will fail!\n";
    for(int i=0; i<x; i++){
        cout<<"Please enter the value for index "<<i+1<<": "; cin>>temp; vec.push_back(temp);
    }
    int searchValue; cout<<"Please enter the value you'd like to search for: "; cin>>searchValue;
    searchValue = search(vec, searchValue);
    if(searchValue == -1)
        cout<<"Your search value isn't present in the array\n";
    else
        cout<<"Your search value is in the index "<<searchValue+1<<" of the array\n";
}