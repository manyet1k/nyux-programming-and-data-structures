//#include <bits/stdc++.h>
//using namespace std;
bool is_palindrome(int test){
    short digits;
    for(short i=1; i<=10; i++){
        if((int)(test/pow(10,i))==0){
            digits=i;
            break;
        }
    }
    short leftmostDigit = test/pow(10,digits-1);
    //cout<<(test-(test%10 + leftmostDigit * pow(10,digits-1)))/10<<"\n";
    if(leftmostDigit == test % 10){
        if(digits <= 2)
            return 1;
        else
            return is_palindrome((test-(test%10 + leftmostDigit * pow(10,digits-1)))/10);
    } else return 0;
}
/*int main(){
    int x; cin>>x;
    if(is_palindrome(x))
        cout<<"Palindrome\n";
    else
        cout<<"Not palindrome\n";
}*/