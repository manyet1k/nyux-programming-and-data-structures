//#include <iostream>
using namespace std;
int abs(int x){
    if (x<0)
        return x*-1;
    else
        return x;
}
int maxabsinlst(int lst[], int size){
    int largestValue = abs(lst[0]);
    for (int i=0;i<size-1;i++){
        //cout<<"Abs of lst[i+1] == "<<abs(lst[i+1])<<endl;
        if (abs(lst[i+1])>largestValue)
            largestValue = abs(lst[i+1]);
    }
    return largestValue;
}/*
int main(){
    int lst[6] = {-19, -3, 20, -1, 5, -25};
    cout<<maxabsinlst(lst, 6)<<endl;
}*/