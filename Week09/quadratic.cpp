#include <iostream>
#include <math.h>
using namespace std;
const short NO_SOLUTION = 0;
const short NO_REAL_SOLUTION = 1;
const short ONE_REAL_SOLUTION = 2;
const short TWO_REAL_SOLUTIONS = 3;
const short ALL_REAL_SOLUTIONS = 4;
const short LINEAR_EQUATION = 5;
short quadraticSolver(double a, double b, double c, double& sol1, double& sol2){
    double discriminant = b*b-4*a*c;
    if (a==0 && b==0 && c==0)
        return ALL_REAL_SOLUTIONS;
    else if (a==0 && b==0 && c!=0)
        return NO_SOLUTION;
    else if (a==0)
        return LINEAR_EQUATION;
    else if (discriminant < 0){
        discriminant *= -1;
        sol1 = (sqrt(discriminant)-b)/(2*a);
        sol2 = (-1*sqrt(discriminant)-b)/(2*a);
        return NO_REAL_SOLUTION;
    } else if (discriminant == 0){
        sol1 = (sqrt(discriminant)-b)/(2*a);
        return ONE_REAL_SOLUTION;
    } else if (discriminant > 0){
        sol1 = (sqrt(discriminant)-b)/(2*a);
        sol2 = (-1*sqrt(discriminant)-b)/(2*a);
        return TWO_REAL_SOLUTIONS;
    } else
        return -1;
}
double linearSolver(long x, long y){
    return (-1*y)/x;
}
int main(){
    double quadraticCoefficient, coefficient, constant, x1, x2; short solutionType;
    cout<<"Please enter the coefficients a, b, c of a quadratic equation as in ax² + bx + c: "; cin>>quadraticCoefficient>>coefficient>>constant;
    solutionType = quadraticSolver(quadraticCoefficient,coefficient,constant,x1,x2);
    switch (solutionType) {
    case NO_SOLUTION:
        cout<<"This equation is impossible to solve\n";
        break;
    case ALL_REAL_SOLUTIONS:
        cout<<"All real numbers are possible solutions\n";
        break;
    case ONE_REAL_SOLUTION:
        cout<<"There is only one solution, and that is "<<x1<<endl;
        break;
    case TWO_REAL_SOLUTIONS:
        cout<<"The two real solutions are "<<x1<<" and "<<x2<<endl;
        break;
    case NO_REAL_SOLUTION:
        cout<<"The two complex solutions are "<<x1<<"i and "<<x2<<"i \n";
        break;
    case LINEAR_EQUATION:
        cout<<"That's a linear equation, and its only solution is "<<linearSolver(coefficient,constant)<<endl;
        break;
    default:
        cout<<"Program error\n";
        break;
    }
}