#include <iostream>
using namespace std;
void function(short n){
    n = 4;
    cout<<"In-function value of n == "<<n<<endl;
}
int main(){
    short n=3;
    cout<<"Initial value of n == "<<n<<endl;
    function(n);
    cout<<"The new value of n == "<<n<<endl;
}