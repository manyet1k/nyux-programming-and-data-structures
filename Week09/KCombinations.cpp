#include <iostream>
using namespace std;
int main(){
    int n=0, k=1, factn=1, factk=1, subtraction, factsub=1;   
    while(k>n){
        cout<<"Please enter n and k separated by a space: "; cin>>n>>k;
        if (k>n)
            cout<<"k can't be greater than n\n";
    }
    subtraction = n-k;
    while(n>0){
        factn*=n; n--;
    }
    while(k>0){
        factk*=k; k--;
    }
    while(subtraction>0){
        factsub*=subtraction; subtraction--;
    }
    cout<<"There are "<<factn/(factk*factsub)<<" possible combinations\n";
}