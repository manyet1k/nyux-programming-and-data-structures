#include <iostream>
using namespace std;
void analyzeDigits(long num, int& outSum, short& outDigits){
    outSum = 0; outDigits = 0;    
    while(num>1){
        outSum += num % 10;
        num /= 10;
        outDigits++;
    }
}
int main(){
    long input; int sum; short digits; cout<<"Please enter a positive integer: "; cin>>input;
    analyzeDigits(input,sum,digits);
    cout<<digits<<" is the number of digits and their sum is "<<sum<<endl;
}