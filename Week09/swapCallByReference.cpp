#include <iostream>
using namespace std;
void swap(short& x, short& y){
    short temp = y;
    y = x; x = temp;
    cout<<"Swapped x and y, now x == "<<x<<" and y == "<<y<<endl;
}
int main(){
    short x=3, y=5;
    cout<<"Initially, x == "<<x<<" and y == "<<y<<endl;
    swap(x,y);
    cout<<"After the swap, x == "<<x<<" and y == "<<y<<endl;
}