#include <iostream>
using namespace std;
long factorial(int x){ //If we wanted to define the function after the main(), we would write
    long factx=1;      //long factorial(int x) before the main()
    while(x>0){
        factx*=x; x--;
    }
    return factx;
}
long KCombinations(long n, long k){
    long factn, factk, factsub;
    if (k>n){
        cout<<"k can't be greater than n\n";
        return -1;
    }
    factn = factorial(n); //cout<<factn<<" == factn\n";
    factk = factorial(k); //cout<<factk<<" == factk\n";
    factsub = factorial(n-k); //cout<<factsub<<" == factsub\n";
    return factn/(factk*factsub);
}
int main(){
    long n=0, k=1;   
    while(k>n){
        cout<<"Please enter n and k separated by a space: "; cin>>n>>k;
        if (k>n)
            cout<<"k can't be greater than n\n";
    }
    cout<<"There are "<< KCombinations(n,k) <<" possible combinations\n";
}