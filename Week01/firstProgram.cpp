/*First program: takes the sum of 2 given numbers*/
#include <iostream>
using namespace std;
int main(){
    int num1, num2;
    cout << "Please enter 2 numbers separated by a space: ";
    cin >> num1 >> num2;
    cout << num1 << " + " << num2 << " = " << num1 + num2;
    return 0;
}