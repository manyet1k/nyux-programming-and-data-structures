#include <bits/stdc++.h>
#include "openInputFile.h"
using namespace std;
int main(){
    ifstream infile; openInputFile(infile, "textfiles/names.txt");
    string x;
    /*while(infile >> x){
        cout<<x<<'\n';
    }*/ //This isn't quite what we want
    getline(infile, x); //Otherwise the program will print an empty line before the first line
    while(infile){ //Now let's use getline to fetch entire lines
        cout<<x<<'\n';
        getline(infile, x);
    }
    infile.close();
}