using namespace std; //This is a very useful function which we will be using a lot in the future, that's why it's a separate file
void openInputFile(ifstream& file, string filename){
    file.open(filename);
    while(!file){
        cout<<"File "<<filename<<" failed to open, trying again\n";
        file.open(filename);
        if(!file){
            cout<<"File "<<filename<<" failed to open again, please enter the filename again: "; cin>>filename;
        }
    }
}