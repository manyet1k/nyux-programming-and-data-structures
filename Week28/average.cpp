#include <bits/stdc++.h>
#include "openInputFile.h"
using namespace std;
int main(){
    string filename; cout<<"Please enter the name of the file you'd like to open (textfiles/numbers.txt): "; cin>>filename;
    ifstream numbers; openInputFile(numbers, filename);
    vector<int> v; int temp;
    while(numbers >> temp){ //Better than checking EOF
        v.push_back(temp);
    } 
    double sum=0;
    for(int i:v){
        sum += i;
    }
    cout << "The average of all elements is " << sum/v.size() << '\n';
    numbers.close();
}