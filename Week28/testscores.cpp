#include <bits/stdc++.h>
#include "openInputFile.h"
using namespace std;
struct student
{
    string studentId;
    string name;
    short score;
};

int main(){
    ifstream infile; openInputFile(infile, "textfiles/students.txt"); //Disclaimer: All names and numerical values are generated using random.org and behindthename.com/random and have nothing to do with people IRL
    short minimumScore; vector<student> v; student temp;
    cout<<"What would you like the minimum threshold for test scores to be? "; cin>>minimumScore;
    while(infile >> temp.studentId){
        infile >> temp.score;
        getline(infile, temp.name);
        temp.name.erase(temp.name.begin());
        //cout<<"Checking "<<temp.studentId<<' '<<temp.name<<' '<<temp.score<<'\n';
        if(temp.score > minimumScore)
            v.push_back(temp);
    }
    cout<<"The students with a score greater than " << minimumScore << " are:\n\n";
    for (student s: v){
        cout<<s.name<<" ID "<<s.studentId<<'\n';
    }
    infile.close();
}