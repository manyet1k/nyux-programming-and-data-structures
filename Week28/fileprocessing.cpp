#include <bits/stdc++.h>
using namespace std;
int main(){
    ofstream outFile; outFile.open("textfiles/foo.txt"); //from the fstream library
    ofstream outFile2("textfiles/bar.txt"); //constructor
    outFile<<"Hello World\n"; //Erases everything in the file and writes Hello World
    outFile2<<"Hello World\n"; //If the file does not exist, it creates the file
    outFile.close(); outFile2.close();
}