#include <bits/stdc++.h>
using namespace std;
class mathOperations{
    public:
        double multiply(double x, double y){
            return x*y;
        }
        vector<double> multiply(double x, double y, double z, double a){
            return vector<double>{x*z, x*a + y*z, y*a};
        }
};

int main(){
    mathOperations m; char selection='a'; double x,y,z,a;
    while(selection != 'n' && selection != 'b'){
        cout<<"Would you like to enter two numbers of two binomials? (n/b): "; cin>>selection;
        if(selection != 'n' && selection != 'b')
            cout<<"Please input either n for number, or b for binomial\n";
        }
    if(selection == 'n'){
        cout<<"Please input 2 numbers: "; cin>>x>>y;
        cout<<x<<" multiplied by "<<y<<" equals "<<m.multiply(x,y)<<'\n';
    }
    if(selection == 'b'){
        cout<<"Please input 2 binomials (in the order of a, b, c, d for ax+b and cx+d): "; cin>>x>>y>>z>>a;
        vector<double> temp = m.multiply(x,y,z,a);
        cout<<x<<"x+"<<y<<" multiplied by "<<z<<"x+"<<a<<" equals "<<temp[0]<<"x²+"<<temp[1]<<temp[2]<<'\n';
    }
}