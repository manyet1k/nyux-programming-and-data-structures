#include <bits/stdc++.h>
using namespace std;
class person{
    private:
        string name;
    public:
        person* spouse;
        void setName(string name_){
            name = name_;
        }
        void marry(person& newSpouse){
            newSpouse.spouse = this;
            spouse = &newSpouse;
        }
        person getSpouse()const {return *spouse;}
        string getName()const {return name;}
};
int main(){
    person p1, p2;
    p1.setName("p1");
    p2.setName("p2"); //This could've probably been done in a better way that I don't yet know
    p1.marry(p2);
    cout<<"p1's spouse is "<<p1.getSpouse().getName()<<" and p2's spouse is "<<p2.getSpouse().getName()<<'\n';
}