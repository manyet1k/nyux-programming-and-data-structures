#include <bits/stdc++.h>
using namespace std;
class matrix{
    private:
        double a_00;
        double a_01;
        double a_10;
        double a_11;
    public:
        matrix operator+(matrix a){
            matrix temp;
            temp.a_00 = a_00 + a.a_00;
            temp.a_01 = a_01 + a.a_01;
            temp.a_10 = a_10 + a.a_10;
            temp.a_11 = a_11 + a.a_11;
            return temp;
        }
        matrix operator-(matrix a){
            matrix temp;
            temp.a_00 = a_00 - a.a_00;
            temp.a_01 = a_01 - a.a_01;
            temp.a_10 = a_10 - a.a_10;
            temp.a_11 = a_11 - a.a_11;
            return temp;
        }
        matrix operator*(matrix a){
            matrix temp;
            temp.a_00 = a_00*a.a_00 + a_01*a.a_10;
            temp.a_01 = a_00*a.a_01 + a_01*a.a_11;
            temp.a_10 = a_10*a.a_00 + a_11*a.a_10;
            temp.a_11 = a_10*a.a_01 + a_11*a.a_11;
            return temp;
        }
        bool operator==(matrix a){
            if(a.a_00 == a_00 && a.a_01 == a_01 && a.a_10 == a_10 && a.a_11 == a_11)
                return 1;
            else
                return 0;
        }
        void setMatrix(double a, double b, double c, double d){ 
            a_00 = a;
            a_01 = b;
            a_10 = c;
            a_11 = d;
        }
        void printMatrix(){
            cout<<'\n'<<a_00<<'\t'<<a_01<<'\n'<<a_10<<'\t'<<a_11;
        }
};
int main(){
    matrix m1, m2, result; double values[4]; char operation;
    cout << "Please enter the elements of the first 2x2 matrix, in clockwise order beginning from the top left: ";
    cin>>values[0]>>values[1]>>values[3]>>values[2]; m1.setMatrix(values[0],values[1],values[2],values[3]);
    cout << "Please enter the elements of the second 2x2 matrix, in clockwise order beginning from the top left: ";
    cin>>values[0]>>values[1]>>values[3]>>values[2]; m2.setMatrix(values[0],values[1],values[2],values[3]);
    while(operation != '+' && operation != '-' && operation != '*'){
        cout<<"Please enter an operation (+,-,*): "; cin>>operation;
        if (m1 == m2)
            cout<<"Those 2 matrices are equal\n";
        if(operation == '+')
            result = m1 + m2;
        else if (operation == '-')
            result = m1 - m2;
        else if (operation == '*')
            result = m1 * m2;
        else
            cout<<"\nInvalid operation";
    }
    cout<<"The result is: "; result.printMatrix(); cout<<'\n';
}