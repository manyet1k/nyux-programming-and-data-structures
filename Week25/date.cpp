#include <bits/stdc++.h>
using namespace std;
class Date{
    private:
        int day;
        int month;
        int year;
    public:
        //Constructor: Member initialization list
        Date(): day(1), month(1), year(1970){}
        /*Date(){
            day=1;
            month=1;
            year=1970;
        }*/ //For now this is also fine and does the same thing
        Date(int d, int m, int y): day(d), month(m), year(y){}
        //Mutators
        void setDay(int newDay){
            if(newDay > 0 && newDay <= 31)
                day = newDay;
        }
        void setMonth(int newMonth){
            if(newMonth > 0 && newMonth <= 12)
                month = newMonth;
        }
        void setYear(int newYear){
            year = newYear;
        }
        //Accessors
        int getDay()const{return day;}
        int getMonth()const{return month;}
        int getYear()const{return year;}
        void displayDate(int localization){
            switch (localization){
            case 0:
                cout<<month<<'/'<<day<<'/'<<year<<'\n';
                break;
            case 1:
                cout<<day<<'/'<<month<<'/'<<year<<'\n';
                break;
            default:
                cout<<year<<'/'<<month<<'/'<<day<<'\n';
                break;
            }
        }
};
int main(){
    Date today;
    today.setDay(25);
    today.setMonth(7);
    today.setYear(2021);
    cout<<"Today is "; today.displayDate(1);

    Date d1(6, 8, 1991); //Works thanks to the constructor in line 16
}