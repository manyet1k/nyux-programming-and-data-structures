#include <bits/stdc++.h>
using namespace std;
class animal{
    private:
        string color;
    public:
        void setColor(string x){
            color = x;
        }
        string getColor(){
            return color;
        }
};
class cat: public animal{
    public:
    string sound(){
        return "Meow";
    }
};
class dog: public animal{
    public:
    string sound(){
        return "Woof";
    }
};
int main(){
    cat c; dog d;
    c.setColor("White"); d.setColor("Brown");
    cout<<c.sound()<<' '<<d.sound()<<'\n';
    cout<<c.getColor()<<' '<<d.getColor()<<'\n';
}