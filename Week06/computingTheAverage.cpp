#include <iostream>
using namespace std;
int main(){
    cout<<"Please enter the number of the students: "; long numberOfStudents, x; double sumOfGrades=0; short gradeOfLastStudent; cin>>numberOfStudents;
    cout<<"Please enter the grades of each student, separated by spaces: "; x = numberOfStudents;
    while(x--){
        cin>>gradeOfLastStudent;
        sumOfGrades += gradeOfLastStudent;
    }
    cout<<"The mean grade of the students is "<< sumOfGrades / numberOfStudents << endl;
}