#include <iostream>
using namespace std;
int main(){
    cout<<"Please enter a positive integer: "; long userInput; cin>>userInput;
    short x=0; int sumOfDigits=0;
    while(userInput>1){
        sumOfDigits += userInput % 10;
        userInput /= 10;
        x++;
    }
    cout<<x<<" is the number of digits and their sum is "<<sumOfDigits<<endl;
}