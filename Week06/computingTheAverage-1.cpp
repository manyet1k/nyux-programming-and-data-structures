#include <iostream>
using namespace std;
int main(){
    long numberOfStudents=1; double sumOfGrades=0; short gradeOfLastStudent; bool seenEndOfInput = 0;
    cout<<"Please enter the grades of each student, separated by spaces.\nEnter your sequence by typing -1: ";
        cin>>gradeOfLastStudent; //cout<<"First input: "<<gradeOfLastStudent<<endl;
        sumOfGrades += gradeOfLastStudent;// cout<<"Firstly, added "<<gradeOfLastStudent<<" to "<<sumOfGrades<<endl;
    
    while(!seenEndOfInput){
        cin>>gradeOfLastStudent;
        if(gradeOfLastStudent == -1)
            break;
        else{
        //cout<<"Grade of last student: "<<gradeOfLastStudent<<endl;
        sumOfGrades += gradeOfLastStudent; //cout<<" Added "<<gradeOfLastStudent<<" to "<<sumOfGrades<<endl;
        numberOfStudents++;}
    }
    
    cout<<"The mean grade of the students is "<< sumOfGrades / numberOfStudents<<endl; //<< " because " << sumOfGrades << " / " << numberOfStudents << endl;
}