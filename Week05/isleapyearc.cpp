#include <iostream>
#include <stdlib.h>
using namespace std;
int main(int argc, char** argv){
    int userInput = atoi(argv[1]);
    if (userInput % 4 != 0)
        cout<<userInput<<" was not a leap year\n";
    else if (userInput % 100 == 0 && userInput % 400 != 0)
        cout<<userInput<<" was not a leap year\n";
    else
        cout<<userInput<<" was a leap year\n";
    return 0;
}