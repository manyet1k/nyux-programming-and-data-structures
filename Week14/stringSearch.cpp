#include <iostream>
#include <string>
using namespace std;
int main(){
    string str1, str2; short minindex;
    cout<<"Please enter a string: "; cin>>str1;
    cout<<"Please enter a string to be searched in that string: "; cin>>str2;
    cout<<"Please enter a minimum index to start searching (0 if you want to start from the beginning): "; cin>>minindex;
    if (str1.find(str2, minindex) == string::npos)
        cout<<str2<<" isn't anywhere in "<<str1<<endl;
    else
        cout<<str2<<" first appears in the index "<<str1.find(str2, minindex)<<" of "<<str1<<endl;
}