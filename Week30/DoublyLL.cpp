#include <bits/stdc++.h>
using namespace std;
template<class T> class LLNode{
    public:
        LLNode<T>* previous;
        T data;
        LLNode<T>* next;

        LLNode(){
            previous = nullptr;
            next = nullptr;
        }
        LLNode(T constrData){
            previous = nullptr;
            data = constrData;
            next = nullptr;
        }
};
template<class T> class LList{
    public:
        LLNode<T>* head = nullptr;
        LLNode<T>* tail = nullptr;

        void append(T appendData){
            LLNode<T>* newnode = new LLNode<T>;
            newnode->data = appendData;
            newnode->next = nullptr;
            if(tail == nullptr){
                newnode->previous = nullptr;
                head = newnode;
            } else {
                newnode->previous = tail;
                tail->next = newnode;
            }
            tail = newnode;
        }

        void prepend(T prependData){
            LLNode<T>* newnode = new LLNode<T>;
            newnode->previous = nullptr;
            newnode->data = prependData;
            if(head == nullptr){
                newnode->next = nullptr;
                head = newnode;
                return;
            }
            newnode->next = head;
            head->previous = newnode;
            head = newnode;
        }

        void deleteElement(int elementIndex){
            if(elementIndex == 0){
                head = head -> next;
                return;
            }
            LLNode<T>* temp = head;
            while(--elementIndex)
                temp = temp->next;
            temp->next = temp->next->next;
        }

        void deleteFromEnd(int elementIndex){
            if(elementIndex == 0){
                tail = tail -> previous;
                tail->next = nullptr;
                return;
            }
            LLNode<T>* temp = tail;
            while(--elementIndex)
                temp = temp->previous;
            temp->previous = temp->previous->previous;
            temp->previous->next = temp->next;
        }

        void print(){
            LLNode<T>* temp = head;
            while(temp->next != nullptr){
                cout<< temp->data <<' ';
                temp = temp->next;
            }
            cout<<temp->data;
        }
};
int main(){
    LList<int> myLList;
    vector<int> v; int numberOfInts; int temp;
    cout<<"Please enter the number of integers you want in the linked list: "; cin>>numberOfInts;
    while(numberOfInts--){
        cin>>temp; v.push_back(temp);
    }

    for(int i: v){
        myLList.append(i);
    }
    myLList.deleteElement(0);
    myLList.deleteElement(1);
    myLList.prepend(3);
    myLList.deleteFromEnd(0);
    myLList.deleteFromEnd(1);
    myLList.print(); cout<<'\n';
}