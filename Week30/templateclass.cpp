#include <bits/stdc++.h>
using namespace std;
template<class T> class myClass{
    private:
        T data;
    public:
        void setData(T x){
            data = x;
        }
        const T getData(){
            return data;
        }
};
int main(){
    myClass<short> x; myClass<string> y;
    x.setData(42); y.setData("foo");
    cout<<x.getData()<<' '<<y.getData()<<'\n';
}