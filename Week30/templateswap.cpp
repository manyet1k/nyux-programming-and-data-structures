#include <bits/stdc++.h>
using namespace std;
template<class T> void myswap(T& a, T& b){
    T temp=a;
    a=b;
    b=temp;
}
int main(){
    int x=3, y=5;                   myswap(x,y);    cout<<"x="<<x<<" && y="<<y<<'\n';
    double a=3.141, b=1.618;        myswap(a,b);    cout<<"a="<<a<<" && b="<<b<<'\n';
    string c="Hello", d="World";    myswap(c,d);    cout<<"c="<<c<<" && d="<<d<<'\n';
}