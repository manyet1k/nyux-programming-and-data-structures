//The complex implementation of linked lists, shown in the lecture. Don't know why it doesn't compile
#include <bits/stdc++.h>
using namespace std;
template<class T> class LListNode{
    public:
        T data;
        LListNode<T>* next;

        LListNode(T newdata = T(), LListNode<T>* newNext = nullptr): data(newdata), next(newNext) {}
        friend class LList<T>;
};

template<class T> class LList{
    public:
        //Data
        LListNode<T>* head; //The only thing this class needs to store
        LListNode<T>* recursiveCopy(LListNode<T>* rhs);
        
        //Constructors
        LList(){
            head = nullptr;
        }
        LList(const LList& rhs){
            head  = nullptr;
            *this = rhs;
        }
        LList<T>& operator=(const LList<T>& rhs);

        //Destructor
        ~LList(){
            clear();
        }

        //A bunch of functions
        bool isEmpty()const {
            return head==nullptr;
        }
        void insertAtHead(T newdata;);
        T removeFromHead();    
        void clear();
        void insertAtEnd(T newdata);
        void insertAtPoint(LListNode<T>* ptr, T newdata());
        int size()const;
};
int main(){
    
}