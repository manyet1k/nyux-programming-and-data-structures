#include <bits/stdc++.h>
using namespace std;
bool allAreEven(vector<int> vec){
    //cout<<"The first element is "
    if(vec[0]%2==1)
        return 0;
    else{
        if(vec.size() == 1){
            return 1;
        } else{
            vec.erase(vec.begin());
            return(allAreEven(vec));
        }
    }
}
int main(){
    int x; cout<<"Please enter how many integers you'd like to enter: "; cin>>x;
    vector<int> vec;
    while(x--){
        vec.push_back(0);
    }
    for(int i=0; i<vec.size(); i++){
        cout<<"Please enter the integer with index "<<i<<": "; cin>>vec[i];
    }
    if(allAreEven(vec))
        cout<<"All of those integers are even\n";
    else
        cout<<"Not all of those integers are even\n";
}