#include <bits/stdc++.h>
using namespace std;
long long factorial(int input){
    if(input == 0 || input == 1)
        return 1;
    else
        return input * factorial(input-1);
}
int main(){
    int x; cout<<"Please enter an integer: "; cin>>x;
    cout<<factorial(x)<<"\n";
}