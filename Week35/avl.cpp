#include <bits/stdc++.h>
#include "cpluspluslabs.h"
using namespace std;

Node* new_node(int key){
    Node* newnode = new Node;
    newnode->key=key;
    newnode->left=nullptr;
    newnode->right=nullptr;
    newnode->height=0;
    return newnode;
}

void updateNodeHeight(Node* node){ //This one's added by me
    if(node->left == nullptr){
        if(node->right == nullptr){
            node->height = 0;
            return;
        }
        node->height = node->right->height +1;
        return;
    }
    if(node->right == nullptr){
        node->height = node->left->height + 1;
        return;
    }
    node->height = max(node->left->height, node->right->height) +1;
}

Node* insertnb(Node* node, int key){
    stack<Node*> ancestors;
    cout<<"Began insertion - ";
    Node* newnode = new_node(key);
    if(node == NULL){
        cout<<"Returned newnode with value "<<newnode->key<<" because node was NULL\nDone\n\n";
        return newnode;
    }
    cout<<"Didn't return newnode because the first node's value was "<<node->key<<'\n';
    Node* temp = node;
    while(1){
        cout<<"The value of the temp node is "<<temp->key<<'\n';
        ancestors.push(temp);
        if(temp->key < key){
            if(temp->right != nullptr){
                cout<<"Moved temp to the right of "<<temp->key<<" because it's greater than or equal to "<<key<<'\n';
                temp = temp->right;
            }
            else{
                cout<<"Placed newnode to the right of temp, because "<<newnode->key<<" is greater than or equal to "<<temp->key<<"\n";
                temp->right = newnode;
                break;
            }
        }
        else
            if(temp->left != nullptr){
                cout<<"Moved temp to the left of "<<temp->key<<" because it's smaller than "<<key<<'\n';
                temp = temp->left;
            } else {
                cout<<"Placed newnode to the left of temp, because "<<newnode->key<<" is smaller than "<<temp->key<<"\n";
                temp->left = newnode;
                break;
            }
    }

    if(temp->left == nullptr || temp->right==nullptr){
        cout<<"Began increasing the height of ancestor nodes\n";
        Node* i;
        if(!ancestors.empty()) cout<<"Entered height addition loop\n";
        while(!ancestors.empty()){
            i=ancestors.top(); ancestors.pop();
            if(i->left == nullptr || i->right==nullptr || i->height != max(i->left->height, i->right->height)+1){
                cout<<"Increased the height of "<<i->key<<" from "<<i->height<<" to "<<i->height+1<<'\n';
                i->height++;
            } else break;
        }
    }
    cout<<"Done\n\n";
    return node;
}

Node* left_rotate(Node* x){
    Node* newroot = x->right;
    short heightModificationCase;
    if(x->left != nullptr && newroot->left != nullptr)
        heightModificationCase = 0;
    else if(x->left != nullptr && newroot->left == nullptr)
        heightModificationCase = 1;
    else if(x->left == nullptr && newroot->left != nullptr)
        heightModificationCase = 2;
    else //if(x->left == nullptr && newroot->left == nullptr)
        heightModificationCase = 3;

    if(newroot->left == nullptr){
        x->right=nullptr;
    } else{
        x->right=newroot->left;
    }
    newroot->left=x;
    
    if(heightModificationCase == 0){
        updateNodeHeight(x);
    }
    else if(heightModificationCase == 1){
        x->height = x->left->height +1;
    }
    else if(heightModificationCase == 2){
        x->height = x->right->height+1;
    }
    else /*if(heightModificationCase == 3)*/{
        x->height = 0;
    }
    updateNodeHeight(newroot);

    return newroot;
}

Node* right_rotate(Node* x){
    Node* newroot = x->left;
    short heightModificationCase;
    if(x->right != nullptr && newroot->right != nullptr)
        heightModificationCase = 0;
    else if(x->right != nullptr && newroot->right == nullptr)
        heightModificationCase = 1;
    else if(x->right == nullptr && newroot->right != nullptr)
        heightModificationCase = 2;
    else //if(x->right == nullptr && newroot->right == nullptr)
        heightModificationCase = 3;

    if(newroot->right == nullptr){
        x->left=nullptr;
    } else{
        x->left=newroot->right;
    }
    newroot->right=x;
    
    if(heightModificationCase == 0){
        updateNodeHeight(x);
    }
    else if(heightModificationCase == 1){
        x->height = x->right->height +1;
    }
    else if(heightModificationCase == 2){
        x->height = x->left->height+1;
    }
    else /*if(heightModificationCase == 3)*/{
        x->height = 0;
    }
    updateNodeHeight(newroot);

    return newroot;
}

int get_balance(Node* x){
    if(x==nullptr || (x->left == nullptr && x->right == nullptr))
        return 0;

    if(x->left == nullptr)
        return -1*(x->right->height+1);

    if(x->right == nullptr)
        return x->left->height+1;

    //if neither are nullptr
    return x->left->height - x->right->height;
}

Node* balance(Node* node){ //This one's added by me as well
    if(get_balance(node) > 1){
        cout<<"Right rotating "<<node->key<<" because its balance is "<<get_balance(node)<<'\n';
        node = right_rotate(node);
    }
    else if(get_balance(node) < -1){
        cout<<"Left rotating "<<node->key<<" because its balance is "<<get_balance(node)<<'\n';
        node = left_rotate(node);
    }
    else{
        cout<<"No need to balance "<<node->key<<", because its balance is "<<get_balance(node)<<'\n';//<<" (Left subtree height == "<< node->left->height<<" && Right subtree height == "<<node->right->height<<'\n';
    }
    if(node->left != nullptr){
        node->left = balance(node->left);
    }
    if(node->right != nullptr){
        node->right = balance(node->right);
    }

    return node;
}

void updateAllHeights(Node* node){ //Again
    updateNodeHeight(node);
    if(node->left != nullptr){
        updateAllHeights(node->left);
    }
    if(node->right != nullptr){
        updateAllHeights(node->right);
    }
}

Node* insert(Node* node, int key){
    vector<Node*> ancestors;
    cout<<"Began insertion - ";
    Node* newnode = new_node(key);
    if(node == NULL){
        cout<<"Returned newnode with value "<<newnode->key<<" because node was NULL\nDone\n\n";
        return newnode;
    }
    cout<<"Didn't return newnode because the first node's value was "<<node->key<<'\n';
    Node* temp = node;
    while(1){
        cout<<"The value of the temp node is "<<temp->key<<'\n';
        ancestors.push_back(temp);
        if(temp->key < key){
            if(temp->right != nullptr){
                cout<<"Moved temp to the right of "<<temp->key<<" because it's smaller than "<<key<<'\n';
                temp = temp->right;
            }
            else{
                cout<<"Placed newnode to the right of temp, because "<<newnode->key<<" is greater than or equal to "<<temp->key<<"\n";
                temp->right = newnode;
                break;
            }
        }
        else
            if(temp->left != nullptr){
                cout<<"Moved temp to the left of "<<temp->key<<" because it's greater than or equal to "<<key<<'\n';
                temp = temp->left;
            } else {
                cout<<"Placed newnode to the left of temp, because "<<newnode->key<<" is smaller than "<<temp->key<<"\n";
                temp->left = newnode;
                break;
            }
    }

    if(temp->left == nullptr || temp->right==nullptr){
        cout<<"Began increasing the height of ancestor nodes\n";
        Node* i;
        if(!ancestors.empty()) cout<<"Entered height addition loop\n";
        for(Node* i: ancestors){
            if(i->left == nullptr || i->right==nullptr || i->height != max(i->left->height, i->right->height)+1){
                cout<<"Increased the height of "<<i->key<<" from "<<i->height<<" to "<<i->height+1<<'\n';
                i->height++;
            } else break;
        }
    }
    cout<<"Balancing node...\n";
    node = balance(node);
    cout<<"After balancing, the root is "<<node->key<<'\n';
    cout<<"Updating all heights\n";
    updateAllHeights(node);
    cout<<"Done\n\n";
    return node;
}

int main(){
    Node *root = new_node(10); cout<<root->key<<"\n\n";


    Node *root2 = insertnb(NULL, 10);
    root2 = insertnb(root2, 20);
    root2 = insertnb(root2, 30);
    cout<<root2->key<<'\t'<<root2->right->key<<'\t'<<root2->right->right->key<<'\n';
    cout<<root2->height<<'\t'<<root2->right->height<<'\t'<<root2->right->right->height<<'\n';
    
    root2 = left_rotate(root2);
    cout<<"After left rotation:\n";
    cout<<root2->key<<'\t'<<root2->left->key<<'\t'<<root2->right->key<<'\n';
    cout<<root2->height<<'\t'<<root2->left->height<<'\t'<<root2->right->height<<'\n';


    cout<<'\n';
    Node *root3 = insertnb(NULL, 30);
    root3 = insertnb(root3, 20);
    root3 = insertnb(root3, 10);
    cout<<root3->key<<'\t'<<root3->left->key<<'\t'<<root3->left->left->key<<'\n';
    cout<<root3->height<<'\t'<<root3->left->height<<'\t'<<root3->left->left->height<<'\n';
    cout<<"The balance of the root node is "<<get_balance(root3)<<'\n';

    root3 = right_rotate(root3);
    cout<<"After right rotation:\n";
    cout<<root3->key<<'\t'<<root3->left->key<<'\t'<<root3->right->key<<'\n';
    cout<<root3->height<<'\t'<<root3->left->height<<'\t'<<root3->right->height<<'\n';


    cout<<"\n\nRoot 4 insertion begins\n";
    Node *root4 = insert(NULL, 10);
    root4 = insert(root4, 20);
    root4 = insert(root4, 30);
    root4 = insert(root4, 40);
    root4 = insert(root4, 50);
    root4 = insert(root4, 25);
    cout<< root4->left->left->key << ' ';cout << root4->left->key << ' ';cout << root4->left->right->key << ' ';cout << root4->key << ' ';cout << root4->right->key << ' ';cout << root4->right->right->key << '\n';
}
