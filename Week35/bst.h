#include <bits/stdc++.h> //This time I didn't remove the debug lines, because the code is quite delicate and prone to bugs
using namespace std;

class BST{
    public:
        int data;
        BST *left=nullptr, *right=nullptr;
        BST();
        BST(int);
        void insert(int);
        int nth_node(int n);
        void inOrderTraversal();

        void Insert(int insertionData){
            BST* newtree = new BST(insertionData);
            newtree->left = nullptr;
            newtree->right = nullptr;
            BST* temp = this;
            while(1){
                //cout<<"Loop is running for "<<insertionData<<'\n';
                if(insertionData < temp->data){
                    if(temp->left == nullptr){
                        temp->left = newtree;
                        //cout<<"Placed the node with value "<<newtree->data<<" to the left of "<<temp->data<<'\n';
                        break;
                    }

                    //cout<<"Will proceed to the left of "<<temp->data<<'\n';
                    temp = temp->left;
                }
                else{
                    if(temp->right == nullptr){
                        temp->right = newtree;
                        //cout<<"Placed the node with value "<<newtree->data<<" to the right of "<<temp->data<<'\n';
                        break;
                    }
                    //cout<<"Will proceed to the right of "<<temp->data<<'\n';
                    temp=temp->right;
                }
            }
            return;
        }
};

inline BST::BST(int constructorData){
    data = constructorData;
}
vector<int> dataVector;
void BST::inOrderTraversal(){
    if(this != nullptr){
        this->left->inOrderTraversal();
        dataVector.push_back(this->data);
        this->right->inOrderTraversal();
    }
}

inline int BST::nth_node(int n){
    inOrderTraversal();
    vector<int> data = dataVector;
    /*for (int i: data)
        cout<<i<<' ';*/
    cout<<'\n';
    return data[n-1];
}

int main(){
    BST mytree(10);
    //cout<<"Declared tree\n"; cout<<mytree.data<<'\n';
    mytree.Insert(20);
    //cout<<"Inserted 20\n";
    mytree.Insert(30);
    //cout<<"Inserted 30\n";
    mytree.Insert(1);
    //cout<<"Inserted 1\n";
    mytree.Insert(2);
    //cout<<"Inserted 2\n";

    //cout<< mytree.data << ' ' << mytree.left->data << ' ' << mytree.left->right->data << ' ' << mytree.right->data << ' ' << mytree.right->right->data << '\n';

    cout << mytree.nth_node(3) <<'\n';
}