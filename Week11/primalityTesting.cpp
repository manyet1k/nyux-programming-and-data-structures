#include <iostream>
#include <math.h>
using namespace std;
bool isPrime(int num){
    if (num<0)
        return -1;
    else if (num<2)
        return 0;
    else if (num == 2)
        return 1;
    for(int i=2;i<(int)(sqrt(num));i++){
        if (num%i == 0)
            return 0;
    }
    return 1;
}
int main(){
    int userInput;
    cout<<"Please enter a positive integer: "; cin>>userInput;
    if(isPrime(userInput))
        cout<<userInput<<" is a prime number\n";
    else
        cout<<userInput<<" is not a prime number\n";
}