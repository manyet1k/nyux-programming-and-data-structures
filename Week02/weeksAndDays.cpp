#include <iostream>
using namespace std;
const int DAYS_IN_A_WEEK = 7;
int main(){
    int totalDays, weeks, remainderDays;
    cout<<"Please enter the total number of days you've traveled: "; cin>>totalDays;
    weeks=totalDays/DAYS_IN_A_WEEK; remainderDays=totalDays%DAYS_IN_A_WEEK;
    cout<<"That's "<<weeks<<" weeks and "<<remainderDays<<" days\n";
}