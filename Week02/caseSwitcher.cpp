#include <iostream>
using namespace std;
const int CASE_SWITCH_CONSTANT = 32;
int main(){
    char inputCharacter;
    cout<<"Please enter a letter: ";
    cin>>inputCharacter;
    cout<<inputCharacter<<" is "<<(short)inputCharacter<<" in ASCII\n";
    if (97 <= inputCharacter && inputCharacter <= 122){
        cout<<inputCharacter<<" is a lowercase character and "<<char(inputCharacter-CASE_SWITCH_CONSTANT)<<" is its uppercase counterpart\n";
    } else if (65 <= inputCharacter && inputCharacter <= 90){
        cout<<inputCharacter<<" is an uppercase character and "<<char(inputCharacter+CASE_SWITCH_CONSTANT)<<" is its lowercase counterpart\n";
    } else {
        cout<<"That's either not a letter or not in the English alphabet\n";
        }
}