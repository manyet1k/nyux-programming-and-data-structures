#include <iostream>
#include <cmath>
using namespace std;
int main(){
    float radius;
    cout<<"Please enter the radius of your circle (in units): ";
    cin>>radius;
    cout<<"The area of your circle is " << radius * radius * M_PI << " units\n";
}