#include <iostream>
using namespace std;
int main(){
    int x=7, y=2;
    float z = x/y, t = 7/2;
    cout<<"7/2 == " << 7/2 << endl;
    cout<<"for x==7 and y==2, x/y == " << x/y << endl;
    cout<<"7/2 stored in a float == " << t << endl;
    cout<<"x/y storedin a float == " << z << endl;
    float a=7.0, b=2.0;
    cout<<"for a==7.0 and b==2.0 (but them being floats this time), a/b == " << a/b << endl;
    cout<<"7.0 / 2.0 == " << 7.0/2.0 << endl;
    cout<<"7.0 / 2 == " << 7.0/2 << endl;
    cout<<"7 / 2.0 == " << 7/2.0 << endl;
    cout<<"Conclusion: / outputs an int value unless one of the input values is a float/double\n";
    return 0;
}