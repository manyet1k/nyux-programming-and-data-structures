#include <bits/stdc++.h>
using namespace std;
template<class T> class SNQ{ //This data structure can work both as a stack and a queue
    private:
        list<T> data;
    public:
        T top() const{
            return *data.begin();
        }

        void push_front(T pushItem){ //Stack-style push function
            data.push_front(pushItem);
        }
        void push_back(T pushItem){ //Queue-style push function
            data.push_back(pushItem);
        }

        void pop_front(){
            data.pop_front();
        }
        void pop_back(){
            data.pop_back();
        }
        void clear(){
            data.clear();
        }       
        bool is_empty() const{
            return data.empty();
        }
        int size()const {
            return data.size();
        }
};
int main(){
    SNQ<int> snq;
    snq.push_back(1);
    snq.push_back(2);
    snq.push_back(3);
    snq.push_front(4); //Think of the stack-style pushes as people jumping the queue/jumping in line
    snq.push_front(5);
    int x = snq.size();
    for(int i=0; i<x; i++){
        cout<<snq.top();
        snq.pop_front();
    }
    cout<<'\n';
}