#include <bits/stdc++.h>
using namespace std;
template<class T> class Queue{
    private:
        list<T> data;
    public:
        void enqueue(T enqueueItem){
            data.push_back(enqueueItem);
        }
        void dequeue(){
            data.pop_front();
        }
        void clear(){
            data.clear();
        }
        T top() const{
            return *data.begin();
        }
        bool is_empty() const{
            return data.empty();
        }
        int size()const {
            return data.size();
        }
};
int main(){
    Queue<int> Q;
    for (int i=0; i<10; i++){
        Q.enqueue(i);
    }
    for (int i=0; i<10; i++){
        cout<<Q.top()<<'\n';
        Q.dequeue();
    }
}