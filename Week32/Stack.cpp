#include <bits/stdc++.h>
using namespace std;
template<class T> class Stack{
    private:
        list<T> data;
    public:
        void push(T pushItem){
            data.push_front(pushItem);
        }
        void pop(){
            data.pop_front();
        }
        void clear(){
            data.clear();
        }
        T top() const{
            return *data.begin();
        }
        bool is_empty() const{
            return data.empty();
        }
        int size()const {
            return data.size();
        }

};
int main(){
    Stack<int> S;
    for (int i=0; i<10; i++){
        S.push(i);
    }
    for (int i=0; i<10; i++){
        cout<<S.top()<<'\n';
        S.pop();
    }
}