/*#include <bits/stdc++.h>
using namespace std;

#define MAX 1000*/

class Queue {
    public:
        int rear_value;
        int a[MAX]; // Maximum size of Queue
        Queue() { rear_value = -1; }
        bool enqueue(int x);
        int dequeue();
        int front();
        int rear();
};

inline bool Queue::enqueue(int x){
    if(rear_value < MAX){
        rear_value++;
        a[rear_value]=x;
        return 1;
    } else
        return 0;
}

inline int Queue::front(){
    return a[0];
}

inline int Queue::rear(){
    return a[rear_value];
}

inline int Queue::dequeue(){
    int x = front();
    for (int i=1; i<=rear_value; i++)
        a[i-1] = a[i];
    a[rear_value] = 0;
    rear_value--;
    return x;
}

/*int main(){
    Queue myqueue;
    myqueue.enqueue(10);
    printf("%i",myqueue.front());
    printf("%i",myqueue.rear());
    printf("%i",myqueue.dequeue());
}*/