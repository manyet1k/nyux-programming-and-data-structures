/*#include <bits/stdc++.h>
using namespace std;

#define MAX 1000*/

class Stack{
    public:
        int top;
        int a[MAX]; // Maximum size of Stack
        Stack() { top = -1; }
        bool push(int x);
        int pop();
        int peek();
        bool isEmpty();
};

inline bool Stack::push(int x){
    if(top < MAX){
        top++;
        a[top]=x;
        return 1;
    } else
        return 0;
}
inline int Stack::peek(){
    return a[top];
}
inline int Stack::pop(){
    if(top == -1)
        return 0;
    
    int x = a[top];
    a[top] = 0;
    top--;
    return x;
}
inline bool Stack::isEmpty(){
    if (top == -1)
        return 1;
    return 0;
}

/*int main(){
    Stack mystack;
    mystack.push(10);
    printf("%i",mystack.peek());
    printf("%i",mystack.pop());
    if(mystack.isEmpty()) 
        printf("No Data");
    else
        printf("Data");
}*/