#include <bits/stdc++.h>
using namespace std;
vector<int> merge(vector<int> a, vector<int> b){
    vector<int> c;
    while(a.size() != 0 && b.size() != 0){
        if(a[0] > b[0]){
            c.push_back(b[0]);
            b.erase(b.begin());
        } else{
            c.push_back(a[0]);
            a.erase(a.begin());
        }
    }
    while (a.size() != 0){
        c.push_back(a[0]);
        a.erase(a.begin());
    }
    while (b.size() != 0){
        c.push_back(b[0]);
        b.erase(b.begin());
    }
    return c;
}

vector<int> sort(vector<int> x){
    vector<int> left, right;
    if(x.size() <= 1)
        return x;
    for(int i=0; i<x.size()/2; i++)
        left.push_back(x[i]);
    for(int i=x.size()/2; i<x.size(); i++)
        right.push_back(x[i]);
    left = sort(left);
    right = sort(right);
    return merge(left, right);
}

int main(){
    int x; cout<<"Please enter how many integers you'd like to enter: "; cin>>x;
    vector<int> vec;
    for(int i=0; i<x; i++){
        vec.push_back(0);
        cout<<"Please enter the value for index "<<i+1<<": "; cin>>vec[i];
    }
    cout<<"The elements are ordered as ";
    vec = sort(vec);
    for(int i=0; i<vec.size(); i++)
        cout<<vec[i]<<' ';
    cout<<"\n";
}