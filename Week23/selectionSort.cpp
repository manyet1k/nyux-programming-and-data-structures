#include <bits/stdc++.h>
using namespace std;
void sort(int arr[], int arrSize){
    int minimum, minindex;
    for(int i=0; i<arrSize-1; i++){
        minimum = arr[i+1]; minindex = i+1;
        for(int o=i+1; o<arrSize; o++){
            if(arr[o]<minimum){
                minimum = arr[o];
                minindex = o;
            }
        }
        if(minimum<arr[i]){
            arr[minindex] = arr[i];
            arr[i]=minimum;
        }
    }
    for(int i=0; i<arrSize; i++){
        cout<<arr[i]<<" ";
    }
}
int main(){
    int x; cout<<"Please enter how many integers you'd like to enter: "; cin>>x;
    int arr[x];
    for(int i=0; i<x; i++){
        cout<<"Please enter the value for index "<<i+1<<": "; cin>>arr[i];
    }
    cout<<"The elements are ordered as ";
    sort(arr, x);
    cout<<"\n";
}