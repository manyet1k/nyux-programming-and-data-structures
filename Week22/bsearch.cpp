#include <bits/stdc++.h>
using namespace std;
int binary_search(int search_value, int lst[], int elements){
    int low=0, high=elements-1, mid=(low+high)/2, index;
    while(1){
        if(search_value<lst[mid]){
            high = mid-1;
            mid = (low+high)/2;
        } else if(search_value>lst[mid]){
            low = mid+1;
            mid = (low+high)/2;
        } else return mid;
    }
}
int main(){
    int lst[] = {0, 1, 2, 18, 19, 20, 25};
    printf("%i",binary_search(20, lst, 7));
}