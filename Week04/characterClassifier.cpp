#include <iostream>
using namespace std;
int main(){
    char userInput;
    cout<<"Please enter a character: "; cin>>userInput;
    if ((short)userInput <= 31 || (short)userInput == 127)
        cout<<"That is a control character. How the heck did you manage to enter one?\n";
     else if ((short)userInput <= 47 || ((short)userInput<=64 && (short)userInput >= 58) || ((short)userInput<=96 && (short)userInput >= 91) || (short)userInput == 126 || ((short)userInput<=137 && (short)userInput >= 128) || (short)userInput == 139 || ((short)userInput<=156 && (short)userInput >= 145) || ((short)userInput<=191 && (short)userInput >= 161) || (short)userInput == 215 || (short)userInput == 247)
        cout<<"That is a non alphanumeric character/a punctuation mark or something\n";
     else if (((short)userInput<=57 && (short)userInput >= 48))
        cout<<"That is a digit\n";
     else if ((short)userInput<=90 && (short)userInput >= 65)
        cout<<"That's an uppercase letter\n";
     else if (((short)userInput<=122 && (short)userInput >= 97))
        cout<<"That's a lowercase letter\n";
     else
        cout<<"That's a non-English letter\n";
}