#include <iostream>
using namespace std;
int main(){
    cout<<"Please enter a time in the 24 hour format: ";
    int hour24, minute24; char temp; cin>>hour24>>temp>>minute24;
    //cout<<hour24<<endl<<temp<<endl<<minute24<<endl;
    if (hour24 == 0)
        cout<<"That's 12:"<<minute24<<" AM\n";
    else if (1 <= hour24 && hour24 <= 11)
        cout<<"That's "<<hour24<<":"<<minute24<<" AM\n";
    else if (hour24 == 12)
        cout<<"That's 12:"<<minute24<<" PM\n";
    else
        cout<<"That's "<<hour24-12<<":"<<minute24<<" PM\n";
}