#include <iostream>
using namespace std;
int main(){
    cout<<"Please enter the amount of money to convert:\n\n# of dollars: "; int dollars; cin>>dollars;
    cout<<"\n# of cents: "; int cents; cin>>cents;
    int totalMoney = dollars * 100 + cents;
    int quarters = totalMoney / 25;
    int dimes = (totalMoney % 25)/10;
    int nickels = ((totalMoney % 25) % 10)/5;
    int pennies = (((totalMoney % 25) % 10) % 5);
    cout<<"The coins are "<<quarters<<" quarters, "<<dimes<<" dimes, "<<nickels<<" nickels and "<<pennies<<" pennies\n";
}