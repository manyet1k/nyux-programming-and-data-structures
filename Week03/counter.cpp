#include <iostream>
using namespace std;
int main(){
    int quarters, dimes, nickels, pennies;
    cout<<"Please enter the number of coins:\n# of quarters: "; cin>>quarters;
    cout<<"# of dimes: "; cin>>dimes;
    cout<<"# of nickels: "; cin>>nickels;
    cout<<"# of pennies: "; cin>>pennies;

    int totalMoneyInCents = quarters*25 + dimes*10 + nickels*5 + pennies*1;
    cout<<"The total is "<<totalMoneyInCents / 100 << " dollars and " << totalMoneyInCents % 100 << " cents\n";
}